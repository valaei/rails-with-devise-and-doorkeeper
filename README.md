# Devise-OmniAuth-DoorKeeper

I followed up the below links to setup this App:

* [Base App Implementation](https://guides.railsgirls.com/start)
* [Devise Integration](https://guides.railsgirls.com/devise)
* [OmniAuth Integration](https://dev.to/geraldarzy/omniauth-with-devise-3d5h)

Also the following tutorial is useful in case you want to setup your own OAuth2 client (Instead of OmniAuth):

* [OAuth2 Client Implementation](https://backend.turing.edu/module3/lessons/intro_to_oauth)

In order to setup the project you need to Install Gems, Migrate DB and setup your OAuth2 App Github credentials:

```
bundle install
bundle exec rails db:migrate
EDITOR="vim" bundle exec rails credentials:edit
```

And then add your credentials like:

```
github:
  github_client_id: #paste your id here
  github_client_secret: #paste your secret here
```

Start Rails Server:
```
bundle exec rails server
```
